# Flawless UI

![alt tag](main_output_optimized.gif)
(earlier version)

### JLTS Scene (Team logo Scene)
- White background
- Fade in for 1 sec
- Display for 2 sec
- Fade out for 1 sec
- Go to **MainMenu** scene immidiately after fade out completed
- Users can press ESC anytime to ignore the team display scene)

### MainMenu Scene
- No background (yet)
- New Game button -> should lead to another choosing difficulty scene?
- Continue button
- Options button -> **OptionsMenu** scene
- Credits button -> **Credits** scene
- Exit button -> Application.Quit()
- Music: *main.ogg*

### OptionsMenu Scene
- No background (yet)
- Music slider control music volume + mute toggle
- Sound slider control sound volume + mute toggle
- No music (use **MainMenu**'s music)

### Credits Scene
- No background (yet)
- Nothing yet. Could be 2-3 text animations fade in and fade out cycle.
- Music: *credits.ogg*
