﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeMenu : MonoBehaviour
{
    public Button button;
    public string changeSceneName;

    void Start()
    {
        if (button != null)
            button.onClick.AddListener(LoadScene);
    }

    public void LoadScene()
    {
        if (changeSceneName == "Exit")
        {
            Application.Quit();
            return;
        }

        GameObject fader = GameObject.FindGameObjectWithTag("Fader");
        if (fader != null)
        {
            fader.BroadcastMessage("Enable");
            fader.BroadcastMessage("FadeOut");
            Invoke("LoadSceneImmidiately", .23f); //a bit faster than fade out time (0.25)
            return;
        }
        else LoadSceneImmidiately();
    }

    void LoadSceneImmidiately()
    {
        Application.LoadLevel(changeSceneName);
        for (int i = 0; i < SoundManager.instance.backgroundMusicsName.Length; ++i)
        {
            if (SoundManager.instance.backgroundMusicsName[i] == changeSceneName)
            {
                if (SoundManager.instance.musicSource.clip == SoundManager.instance.backgroundMusics[i])
                    break;
                SoundManager.instance.musicSource.Stop();
                SoundManager.instance.musicSource.clip = SoundManager.instance.backgroundMusics[i];
                SoundManager.instance.musicSource.Play();
            }
        }
    }
}
