﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectableControl : MonoBehaviour, IPointerEnterHandler, ISelectHandler, IDeselectHandler, IPointerExitHandler
{
    public Color baseSelectedColor = Color.white;
    public int selectSound = 1;

    private Selectable selectable;
    private Color normalButtonImageColor;
    private EventSystem evtSys;
    private KeyboardUIControl kbControl;

    void Awake()
    {
        selectable = GetComponent<Selectable>();
        normalButtonImageColor = selectable.image.color;
        evtSys = GameObject.Find("EventSystem").GetComponent<EventSystem>();
        kbControl = transform.parent.GetComponent<KeyboardUIControl>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        selectable.image.color = baseSelectedColor;
        evtSys.SetSelectedGameObject(null);
        kbControl.hasSelected = false;
        SoundManager.instance.PlaySingle(SoundManager.instance.buttonSelect1);
    }

    public void OnSelect(BaseEventData eventData)
    {
        selectable.image.color = baseSelectedColor;
        if (!kbControl.hasSelected)
            SoundManager.instance.PlaySingle(SoundManager.instance.buttonSelect2);
        else
            SoundManager.instance.PlaySingle(SoundManager.instance.buttonSelect1);
        //kbControl.hasSelected = true;
    }

    public void OnDeselect(BaseEventData eventData)
    {
        selectable.image.color = normalButtonImageColor;
        //kbControl.hasSelected = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        selectable.image.color = normalButtonImageColor;
    }
}
