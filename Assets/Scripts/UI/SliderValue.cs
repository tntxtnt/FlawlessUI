﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SliderValue : MonoBehaviour
{
    public Slider slider;
    public Text text;

    void Start()
    {
        // register listener once
        slider.onValueChanged.AddListener(ChangeValue);
        OnEnable();
    }

    void OnEnable()
    {
        // weird NullValueReference bug?
        if (GameSettings.instance == null) return;

        // get value from game settings
        if (slider.name.Contains("Music"))
            slider.value = GameSettings.instance.musicVolume * 100f;
        else if (slider.name.Contains("Sound"))
            slider.value = GameSettings.instance.sfxVolume * 100f;

        // update slider value status
        text.text = ((int)slider.value).ToString();
        text.color = new Color(1f, 1f, 1f, 1f);
    }

    void OnDisable()
    {
        // fade text
        text.color = new Color(1f, 1f, 1f, 0.25f);
    }

    void ChangeValue(float value)
    {
        // update slider text
        text.text = ((int)value).ToString();

        // update game settings
        if (slider.name.Contains("Music"))
            GameSettings.instance.ChangeMusicVolume(value / 100f);
        else if (slider.name.Contains("Sound"))
            GameSettings.instance.ChangeSfxVolume(value / 100f);
    }
}
