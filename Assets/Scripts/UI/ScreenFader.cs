﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenFader : MonoBehaviour
{
    public Image fader;

    private Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void Enable()
    {
        fader.enabled = true;
    }

    public void Disable()
    {
        fader.enabled = false;
    }

    public void FadeOut()
    {
        anim.SetTrigger("fadeOut");
    }

    public void FadeIn()
    {
        anim.SetTrigger("fadeIn");
    }
}

