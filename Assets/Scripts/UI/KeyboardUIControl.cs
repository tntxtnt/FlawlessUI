﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class KeyboardUIControl : MonoBehaviour
{
    public Selectable defaultSelectable;

    [HideInInspector]
    public bool hasSelected = false;

    private EventSystem eventSystem;

    void Awake()
    {
        eventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();
    }

    void Start()
    {
        //selectables[defaultSelectId].Select();
    }

    void Update()
    {
        if (Input.GetButtonDown("Vertical") && !hasSelected)
        {
            hasSelected = true;
            defaultSelectable.Select();
        }
        if (Input.GetButtonDown("Submit") && hasSelected)
            SoundManager.instance.PlaySingle(SoundManager.instance.buttonSelect2);
        if (Input.GetAxis("Mouse X") > 0f || Input.GetAxis("Mouse Y") > 0f)
        {
            if (hasSelected)
                eventSystem.SetSelectedGameObject(null);
            hasSelected = false;
        }
    }
}
