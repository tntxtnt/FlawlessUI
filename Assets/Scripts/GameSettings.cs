﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour
{
    public float musicVolume = 0.7f;
    public bool muteMusic = false;
    public float sfxVolume = 1f;
    public bool muteSfx = false;

    public static GameSettings instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        ChangeMusicVolume(musicVolume);
        ToggleMusic(muteMusic);
        ChangeSfxVolume(sfxVolume);
        ToggleSfx(muteSfx);
    }

    public void ChangeMusicVolume(float volume)
    {
        SoundManager.instance.musicSource.volume = musicVolume = volume;
    }

    public void ToggleMusic(bool mute)
    {
        SoundManager.instance.musicSource.mute = muteMusic = mute;
    }

    public void ChangeSfxVolume(float volume)
    {
        SoundManager.instance.sfxSource.volume = sfxVolume = volume;
    }

    public void ToggleSfx(bool mute)
    {
        SoundManager.instance.sfxSource.mute = muteSfx = mute;
    }
}
