﻿using UnityEngine;
using System.Collections;

public class JLTS : MonoBehaviour
{
    private bool jlts = true;

    private Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }
    
    void FadeOut()
    {
        anim.SetTrigger("fadeOut");
    }

    public void FadeOutAfter(float seconds)
    {
        Invoke("FadeOut", seconds);
    }

    public void MainMenu()
    {
        Application.LoadLevel("MainMenu");
    }

    public void WaitUntilEscape()
    {
        StartCoroutine(IWaitUntilEscape());
    }

    public IEnumerator IWaitUntilEscape()
    {
        while (jlts)
        {
            if (Input.anyKeyDown)
                break;
            yield return Time.fixedDeltaTime;
        }
        MainMenu();
    }
}
